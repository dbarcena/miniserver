# MINISERVER

Simple http server.

## Binarys

miniserver.exe version win 64
miniserver.lnx versión linux 64

## Build

go build miniserver.go for Windows.
go build -o miniserver.lnx miniserver.go for linux or osx.

## Help

```
miniserver.exe --help
./miniserver.lnx --help
```

## Indicate a different route to ./

```
miniserver.exe --path="c:\test"
./miniserver.lnx --path="./home/test"
```

## Specify a port other than 8080

```
miniserver.exe --port=80
./miniserver.lnx --port=80
```
