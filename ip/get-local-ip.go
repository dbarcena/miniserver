package ip

import (
	"net"
	"os"
)

func GetLocalIp() string {

	// https://stackoverflow.com/users/2744842/eric-gilbertson

	host, _ := os.Hostname()
	addrs, _ := net.LookupIP(host)
	for _, addr := range addrs {
		if ipv4 := addr.To4(); ipv4 != nil {
			return ipv4.String()
		}
	}
	return ""
}
