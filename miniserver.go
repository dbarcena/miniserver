package main

import (
	"flag"
	"fmt"
	"miniserver/ip"
	"os"

	"log"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {

	fmt.Println("Versión 0.02 2022/06/24")

	helpPtr := flag.Bool("help", false, "Help")
	pathPtr := flag.String("path", "./", "Path static file")
	portPtr := flag.Int("port", 8080, "Port listen")

	flag.Parse()

	if *helpPtr == true {
		flag.PrintDefaults()
		os.Exit(1)
	}

	if _, err := os.Stat(*pathPtr); err != nil {
		log.Println(err)
		os.Exit(1)
	}

	e := echo.New()
	e.HideBanner = true

	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "method=${method}, uri=${uri}, status=${status}\n",
	}))

	e.Use(middleware.Recover())
	e.Use(middleware.Gzip())

	//e.Static("/index.html", filepath.Join( *pathPtr, "index.html"))

	e.Static("/", *pathPtr)

	port_str := fmt.Sprintf(":%d", *portPtr)

	fmt.Printf("http://%s%s\n", "localhost", port_str)
	fmt.Printf("http://%s%s\n", ip.GetLocalIp(), port_str)

	if err := e.Start(port_str); err != nil {
		log.Println(err)
	}
}
